<?php

namespace App\Tests\Functional;
use App\Entity\Voiture;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class VoitureTest extends WebTestCase
{
public function testShouldDisplayVoitureIndex()
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/voiture');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Voiture index');
    }



        public function testShouldDisplayCreateNewVoiture()
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/voiture/new');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Create new Voiture');
    }


    public function testShouldAddNewVoiture()
    {
    $client = static::createClient();
    $client->followRedirects();

    // Accéder au formulaire de création de voiture
    $crawler = $client->request('GET', '/voiture/new');

    // Renseigner les champs du formulaire
    $uuid = uniqid();
    $form = $crawler->selectButton('Save')->form([
        'voiture[Serie]' => 'Serie Test ' . $uuid,
        'voiture[Date_Mise_En_Marche][day]' => '27',
        'voiture[Date_Mise_En_Marche][month]' => '12',
        'voiture[Date_Mise_En_Marche][year]' => '2023',
        'voiture[Modele]' => 'Modele Test ' . $uuid,
        'voiture[Prix_Jour]' => 100, // Exemple de prix
    ]);

    // Soumettre le formulaire
    $client->submit($form);

    // Vérifier la réponse
    $this->assertResponseIsSuccessful();
    $this->assertSelectorTextContains('body', 'Serie Test ' . $uuid);
    $this->assertSelectorTextContains('body', 'Modele Test ' . $uuid);
   }

public function testShouldUpdateVoiture()
{
    $client = static::createClient();
    $client->followRedirects();

    // Accéder au formulaire de modification de voiture
    $crawler = $client->request('GET', '/voiture/1/edit');

    // Renseigner les champs du formulaire
    $uuid = uniqid();
    $form = $crawler->selectButton('Update')->form([
        'voiture[Serie]' => 'Serie Test ' . $uuid,
        'voiture[Date_Mise_En_Marche][day]' => '27',
        'voiture[Date_Mise_En_Marche][month]' => '12',
        'voiture[Date_Mise_En_Marche][year]' => '2023',
        'voiture[Modele]' => 'Modele Test ' . $uuid,
        'voiture[Prix_Jour]' => 100, // Exemple de prix
    ]);

    // Soumettre le formulaire
    $client->submit($form);

    // Vérifier la réponse
    $this->assertResponseIsSuccessful();
    $this->assertSelectorTextContains('body', 'Serie Test ' . $uuid);
    $this->assertSelectorTextContains('body', 'Modele Test ' . $uuid);

    // Vérifier que la voiture a été mise à jour dans la base de données
    $voiture = $this->getContainer()->get('doctrine')->getRepository(Voiture::class)->findOneById(1);
    $this->assertEquals('Serie Test ' . $uuid, $voiture->getSerie());
    $this->assertEquals('2023-12-27', $voiture->getDateMiseEnMarche()->format('Y-m-d'));
    $this->assertEquals('Modele Test ' . $uuid, $voiture->getModele());
    $this->assertEquals(100, $voiture->getPrixJour());
    }
}
