<?php

namespace App\Tests\Unit;

use App\Entity\Voiture;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use DateTime;

class VoitureTest extends KernelTestCase
{
    public function testIsTrue()
    {
	$voiture = new Voiture();
	$datetime = new DateTime();

	$voiture->setSerie('ABC123')
		->setDateMiseEnMarche($datetime)
		->setModele('Model XYZ')
		->setPrixJour(100.50);

	$this->assertTrue($voiture->getSerie() === 'ABC123');
	$this->assertTrue($voiture->getDateMiseEnMarche() === $datetime);
	$this->assertTrue($voiture->getModele() === 'Model XYZ');
	$this->assertTrue($voiture->getPrixJour() === 100.50);
     }

     public function testIsFalse()
     {
        $voiture = new Voiture();
	$datetime = new DateTime();

        $voiture->setSerie('ABC123')
                ->setDateMiseEnMarche($datetime)
                ->setModele('Model XYZ')
                ->setPrixJour(100.50);

        $this->assertFalse($voiture->getSerie() === 'xyz456');
        $this->assertFalse($voiture->getDateMiseEnMarche() === new DateTime());
        $this->assertFalse($voiture->getModele() === 'abc');
        $this->assertFalse($voiture->getPrixJour() === 255.50);
     }

     public function testIsEmpty()
     {
        $voiture = new Voiture();

        $this->assertEmpty($voiture->getSerie());
        $this->assertEmpty($voiture->getDateMiseEnMarche());
        $this->assertEmpty($voiture->getModele());
        $this->assertEmpty($voiture->getPrixJour());
     }

}
