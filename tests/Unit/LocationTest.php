<?php

namespace App\Tests\Unit;

use App\Entity\Location;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use DateTime;

class LocationTest extends KernelTestCase
{
    public function testIsTrue()
    {
        $location = new Location();
        $datetimeDebut = new DateTime();
        $datetimeRetour = new DateTime();

        $location->setDateDebut($datetimeDebut)
            ->setDateRetour($datetimeRetour)
            ->setPrix(150.75);

        // Assurez-vous que les valeurs définies sont égales à celles récupérées
        $this->assertTrue($location->getDateDebut() === $datetimeDebut);
        $this->assertTrue($location->getDateRetour() === $datetimeRetour);
        $this->assertTrue($location->getPrix() === 150.75);
    }

    public function testIsFalse()
    {
        $location = new Location();
        $datetimeDebut = new DateTime();
        $datetimeRetour = new DateTime();

        $location->setDateDebut($datetimeDebut)
            ->setDateRetour($datetimeRetour)
            ->setPrix(150.75);

        // Assurez-vous que les valeurs définies ne sont pas égales à d'autres valeurs
        $this->assertFalse($location->getDateDebut() === new DateTime());
        $this->assertFalse($location->getDateRetour() === new DateTime());
        $this->assertFalse($location->getPrix() === 200.50);
    }

    public function testIsEmpty()
    {
        $location = new Location();

        // Assurez-vous que les valeurs sont vides par défaut
        $this->assertEmpty($location->getDateDebut());
        $this->assertEmpty($location->getDateRetour());
        $this->assertEmpty($location->getPrix());
    }

}

