<?php

namespace App\Tests\Unit;

use App\Entity\Client;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ClientTest extends KernelTestCase
{
    public function testIsTrue()
    {
        $client = new Client();
        $client->setCin(123456789)
            ->setNom('Benfarah')
            ->setPrenom('Nada')
            ->setAdresse('Cité Elwouroud2 3040');

        // Assurez-vous que les valeurs définies sont égales à celles récupérées
        $this->assertTrue($client->getCin() === 123456789);
        $this->assertTrue($client->getNom() === 'Benfarah');
        $this->assertTrue($client->getPrenom() === 'Nada');
        $this->assertTrue($client->getAdresse() === 'Cité Elwouroud2 3040');
    }

    public function testIsFalse()
    {
        $client = new Client();
        $client->setCin(123456789)
            ->setNom('Benfarah')
            ->setPrenom('Nada')
            ->setAdresse('Cité Elwouroud2 3040');

        // Assurez-vous que les valeurs définies ne sont pas égales à d'autres valeurs
        $this->assertFalse($client->getCin() === 987654321);
        $this->assertFalse($client->getNom() === 'Benkhlifa');
        $this->assertFalse($client->getPrenom() === 'Naziha');
        $this->assertFalse($client->getAdresse() === 'Ariana Soghra 2033');
    }

    public function testIsEmpty()
    {
        $client = new Client();

        // Assurez-vous que les valeurs sont vides par défaut
        $this->assertEmpty($client->getCin());
        $this->assertEmpty($client->getNom());
        $this->assertEmpty($client->getPrenom());
        $this->assertEmpty($client->getAdresse());
    }

}

